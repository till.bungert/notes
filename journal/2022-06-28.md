# June 28, 2022

## Morning Brainstorm

![](2022-06-28.png)

## Summary

- Tests expanded, confid scores fully unit tested
- Confid scores now properly handle errors
- Metrics still incomplete -> test cases depend on confid scores
- Also test outputting?
- Add test coverage to the readme somewhere?

## Notes

- How do we handle nan? When to error out, when do we propagate?
