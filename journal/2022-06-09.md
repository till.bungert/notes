# June 09, 2022

## Morning Brainstorm

![](2022-06-09.png)

## Summary

- Found out how to implement structured configs
- Implemented structured config for `config.yaml`
- Implemented test to ensure old configs are recognized as valid
