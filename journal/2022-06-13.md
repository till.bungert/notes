# June 13, 2022

## Morning Brainstorm

![](2022-06-13.png)

## Summary

- Finished implementing structured configs
- Fixed all issues with existing yamls in project
- Began adapting models and dataset loading to altered configs
