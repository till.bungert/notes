# July 15, 2022

## Morning Brainstorm

![](2022-07-15.png)

## Summary

- crude config migration, might need some work before we do irreversible stuff to the experiments
- improve output validation, existance is not enough
- begin work on canonical experiment names
