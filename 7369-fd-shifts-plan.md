# FD-Shifts Plan

## PhD Training

- Create concrete plan
- Own your project through and through
- Track your work and improve time estimates
- Do quality control and ask for help if you do not understand something

## TODOs Until Publications

```mermaid
gantt
    title                               Get Publication Ready 🚀
    dateFormat                          YYYY-MM-DD
    axisFormat                          %b-%d
    excludes                            weekends, 2022-06-01, 2022-06-02, 2022-06-03, 2022-06-06, 2022-06-07, 2022-06-20, 2022-06-21, 2022-06-22, 2022-07-07, 2022-07-08

    section                             Code Cleanup
    Go through code                     :active, c1, 2022-05-30, 2d
    Fix up everything                   :c2, after c1, 18d

    section                             Finish Results
    List and Validate Experiments       :c3, after c2, 2d
    Rerun missing/broken Exps           :after c3, 20d
    Rerun missing 64bit inference       :after c3, 20d
```

### Code Cleanup

- [x] Work through code and identify error-prone parts
- [x] Validate experiment configurations
- [x] Add error handling

#### Files To Check

- [x]  `README.md`
- [x]  `example.env`
- [x]  `fd_shifts/__init__.py`
- [x]  `fd_shifts/analysis/__init__.py`
- [x]  `fd_shifts/analysis/confid_scores.py`
- [x]  `fd_shifts/analysis/eval_utils.py`
- [x]  `fd_shifts/analysis/metrics.py`
- [x]  `fd_shifts/analysis/studies.py`
- [x]  `fd_shifts/configs/__init__.py`
- [x]  `fd_shifts/exec.py`
- [x]  `fd_shifts/loaders/abstract_loader.py`
- [x]  `fd_shifts/loaders/dataset_collection.py`
- [x]  `fd_shifts/models/callbacks/__init__.py`
- [x]  `fd_shifts/models/callbacks/confid_monitor.py`
- [x]  `fd_shifts/models/callbacks/training_stages.py`
- [x]  `fd_shifts/models/networks/__init__.py`
- [x]  `fd_shifts/models/networks/confidnet.py`
- [x]  `fd_shifts/models/networks/devries_network.py`
- [x]  `fd_shifts/models/networks/dgvgg.py`
- [x]  `fd_shifts/models/networks/mnist_mlp.py`
- [x]  `fd_shifts/models/networks/mnist_small_conv.py`
- [x]  `fd_shifts/models/networks/residual_flows/__init__.py`
- [x]  `fd_shifts/models/networks/resnet50_imagenet.py`
- [x]  `fd_shifts/models/networks/svhn_small_conv.py`
- [x]  `fd_shifts/models/networks/vgg.py`
- [x]  `fd_shifts/models/networks/vgg16.py`
- [x]  `fd_shifts/models/networks/vgg_devries.py`
- [x]  `fd_shifts/models/networks/vit.py`
- [x]  `fd_shifts/models/networks/zhang_network.py`
- [x]  `fd_shifts/models/confidnet_model.py`
- [x]  `fd_shifts/models/det_mcd_model.py`
- [x]  `fd_shifts/models/devries_model.py`
- [x]  `fd_shifts/models/vit_model.py`
- [x]  `fd_shifts/models/zhang_model.py`
- [x]  `fd_shifts/reporting/__init__.py`
- [x]  `fd_shifts/reporting/__main__.py`
- [x]  `fd_shifts/reporting/data.py`
- [x]  `fd_shifts/reporting/plots.py`
- [x]  `fd_shifts/reporting/tables.py`
- [x]  `notebooks/final_results.ipynb`
- [x]  `fd_shifts/tests/test_analysis.py`
- [x]  `fd_shifts/tests/test_reporting.py`
- [x]  `fd_shifts/utils/__init__.py`
- [x]  `fd_shifts/utils/aug_utils.py`
- [x]  `fd_shifts/utils/exp_utils.py`
- [x]  `launcher/__init__.py`
- [x]  `launcher/breeds_paper_sweep.py`
- [x]  `launcher/camelyon_paper_sweep.py`
- [x]  `launcher/cifar100_paper_sweep.py`
- [x]  `launcher/cifar10_paper_sweep.py`
- [x]  `launcher/supercifar_paper_sweep.py`
- [x]  `launcher/svhn_paper_sweep.py`
- [x]  `launcher/vit_paper_sweep.py`
- [x]  `launcher/wilds_animals_paper_sweep.py`
- [x]  `scripts/__init__.py`
- [x]  `scripts/analyze_msr_pe.ipynb`
- [x]  `scripts/analyze_msr_pe.py`
- [x]  `scripts/do_analysis.py`
- [x]  `scripts/fix_openset_holdout_classes.py`
- [x]  `scripts/gather_tests.py`
- [x]  `scripts/gather_tests_base.py`
- [x]  `scripts/generate_pip_deps_from_conda.py`
- [x]  `scripts/investigate_pe_msr.py`
- [x]  `scripts/kill_jobs.py`
- [x]  `scripts/list_vit_runs.py`
- [x]  `scripts/marry_16_and_64.py`
- [x]  `scripts/merge_32_64.py`
- [x]  `scripts/merge_32_validation.py`
- [x]  `scripts/patch_configs.py`
- [x]  `scripts/playground.py`
- [x]  `scripts/run_cnn_inference.py`
- [x]  `scripts/to_standardized_name.py`
- [x]  `pyproject.toml`
- [x]  `setup.cfg`

#### Collected TODOs

- [ ]  `README.md`
  - [ ]  `TODO: Final pass over readme`                                                    2
  - [ ]  `TODO: Copy over data folder structure`                                           1
- [x]  `fd_shifts/analysis/__init__.py`
  - [o]  `TODO: Finish cleaning this up`                                                   8
  - [x]  `FIX: Add error checking`                                                         4
  - [x]  `FIX: Remove too specific assumptions`                                            4
- [x]  `fd_shifts/analysis/confid_scores.py`
  - [x]  `TODO: Add better error handling/reporting`                                       1
- [o]  `fd_shifts/analysis/eval_utils.py`
  - [x]  `BUG: Replace -1 as a failure marker`                                             1
  - [x]  `NOTE: Use NaN? Explicitly error? Clearer warning?`
  - [x]  `TODO: Better error handling in general`                                          4
  - [o]  `TODO: Clean this up`                                                             4
- [x]  `fd_shifts/analysis/metrics.py`
  - [x]  `TODO: Better error handling`                                                     2
- [o]  `fd_shifts/analysis/studies.py`
  - [x]  `TODO: Better error handling`                                                     2
  - [o]  `FIX: Move noise study destructuring to the dataset`                              2
- [x]  `fd_shifts/configs/__init__.py`
  - [x]  `TODO: Implement structured config`                                               8
  - [x]  `TODO: Implement validation`                                                      4
- [x]  `fd_shifts/exec.py`
  - [x]  `TODO: Handle better configs`                                                     2
  - [x]  `TODO: Handle mode better`                                                        2
  - [x]  `TODO: Log git commit`
  - [o]  `TODO: Don't hard-code number of total classes and number of holdout classes`     1
- [o]  `fd_shifts/loaders/abstract_loader.py`
  - [o]  `TODO: Go over this and make it less presumptuous`                                4
- [o]  `fd_shifts/loaders/dataset_collection.py`
  - [o]  `TODO: Handle configs better`                                                     2
  - [x]  `TODO: Refactor a bit`                                                            2
- [o]  `fd_shifts/models/__init__.py`
  - [o]  `TODO: Make all models work the same`                                             2
  - [x]  `TODO: Add some validation`                                                       2
- [x]  `fd_shifts/models/callbacks/__init__.py`
  - [x]  `TODO: Add error handling`                                                        2
  - [x]  `TODO: Handle configs better`                                                     1
- [o]  `fd_shifts/models/networks/__init__.py`
  - [x]  `TODO: Error handling`                                                            2
  - [o]  `TODO: Make explicit arguments`                                                   1
- [x]  `fd_shifts/reporting/__init__.py`
  - [x]  `TODO: Refactor the rest`                                                         8
  - [x]  `TODO: Add error handling`                                                        4
  - [x]  `TODO: Implement sanity checks on final result table`                             4
- [o]  `fd_shifts/tests/__init__.py`
  - [o]  `TODO: Implement unit tests for analysis`                                         8
  - [x]  `TODO: Implement unit tests for reporting`                                        8
  - [o]  `TODO: Implement unit tests for models`                                           8
- [o]  `fd_shifts/tests/test_analysis.py`
  - [o]  `TODO: Implement unit tests as we refactor`
  - [x]  `HACK: The analysis script deduces the exp name from the path`                    1
  - [x]  `HACK: Matplotlib produces inconsistent pngs on different platforms`
- [x]  `fd_shifts/tests/test_reporting.py`
  - [x]  `TODO: Implement unit tests as we refactor`
- [ ]  `launcher/__init__.py`
  - [ ]  `TODO: Refactor these`                                                            4
  - [ ]  `TODO: Make general launch framework`                                             8
- [ ]  `scripts/__init__.py`
  - [ ]  `TODO: Check which of these are still needed`                                     4
  - [ ]  `TODO: Refactor the necessary ones`                                               4
- [x]  `setup.cfg`
  - [x]  `TODO: Check entry points`                                                        1

### Finish Results

- [ ] List all experiments we want to have run
- [ ] Check for done experiments and rerun broken ones
- [ ] Rerun missing or broken inference in 64bit
