# Precision Study Plan

```mermaid
gantt
    title                               Precision Study
    dateFormat                          YYYY-MM-DD
    axisFormat                          %b-%d
    excludes                            weekends

    Run SVHN interference in 16/32bit       :active, c1, 2022-08-01, 2d
    Run Camelyon interference in 16/32bit   :after c1, 3d
    Implement study in reporting            :2022-08-01, 5d

```


